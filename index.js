function arrayGenerator(arrayLength) {
    // validate if array length is greater than zero
    if (arrayLength <= 0) {
        throw new Error('Invalid array length value')
    }

    let arrayOfRandomIntegers = []

    for (let i = 0; i < arrayLength; i++) {
        const randomInteger = Math.floor(Math.random() * 101) // generate random integer from 0 to 100
        arrayOfRandomIntegers.push(randomInteger)
    }

    return arrayOfRandomIntegers
}

function findSmallestOddInteger(arrayOfIntegers) {
    // sort array in ascending order
    arrayOfIntegers.sort(function (a, b) {
        return a - b
    })

    // return the first encountered odd number (this should be the smallest because we sort the array before)
    const smallestOddInteger = arrayOfIntegers.find(function (integer) {
        if (integer % 2 !== 0) {
            return integer
        }
    })

    return smallestOddInteger ? smallestOddInteger : "No smallest odd integer found"
}

function main() {
    try {
        const generatedArray = arrayGenerator(100) // input array length here
        console.log("Array: ", generatedArray) // print generated array
        
        const smallestOddInteger = findSmallestOddInteger(generatedArray)
        console.log("Smallest odd integer: ", smallestOddInteger) // print output
    } catch (err) {
        console.error(err.message)
    }
}

main() // run program